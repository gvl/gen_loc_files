import optparse, os, sys
import csv

from tool_managers import *

DEFAULT_TOOL_DATA_TABLE_CONF = 'tool_data_table_conf.xml'
DEFAULT_UCSC_BUILDS_TXT= 'tool-data/shared/ucsc/builds.txt'
DEFAULT_UCSC_BUILDS_TXT_SKIP=2 
EXEMPTIONS = 'bin,tmp,lengths,equCab2_chrM,microbes,download'
INSPECT_DIR = None

 
## Add new tools here from tool_managers.py
#  "toolname" : (tool_manager, "index_folder")
TOOLS = {
         #add_scores <<< different
         #alignseq <<<< special
         "all_fasta"    : (all_fasta_mngr, "seq"),
         #annotation prof?
         #bfast<<< hard
         #binned -- nodata
         #blast << useold
         "bowtie"       : (bowtie_mngr, "bowtie_index"),
         "bowtie_cs"    : (bowtie_cs_mngr, "bowtie_index"),
         #bowtie2
         "bwa"          : (bwa_mngr, "bwa_index"),
         "bwa_cs"       : (bwa_cs_mngr, "bwa_index"), 
         #ccat
         "codingSnps"   : (codingSnps_mngr, "seq"),
         #encode -- need data
         #faseq -- nodata requires individual chromosome .fa 
         #fastx clipper seqs << illumina one?
         #fundo --special
         "gatk"         : (gatk_mngr, "srma_index"),
         "lastz"        : (lastz_mngr, "seq"),
         #liftover <<<<< special
         #maf very special
         #microbial -- nodata
         "mosaik"       : (mosaik_mngr, "mosaik_jump"),
         "ngs_sim"      : (ngs_sim_mngr, "seq"),
         #"perm"    : (all_fasta_mngr, "perm_index") -- special
         #"perm_color" <<  : (all_fasta_mngr, "perm_index") -- special
         #phastOdds -- nodata
         "picard"       : (picard_mngr, "srma_index"),
         #quality_scores -- nodata
         #regions -- nodata
         "sam"          : (sam_mngr, "sam_index"),
         "sequence_index_base" : (seq_index_base_mngr, "bwa_index"),
         "sequence_index_color" : (seq_index_color_mngr, "bwa_index"),
         #sift <<< special
         #"srma"         : (srma_mngr, "srma_index"), -- superceeded, actually writes to picard loc
         "twobit"       : (twobit_mngr, "seq")
        }


env = {}
def __main__():
    global env 
    env = AttributeDict(env)

    # command line variables
    usage = "usage: %prog [options] GENOMES_DIR DEST_DIR" 
    parser = optparse.OptionParser(usage=usage)
    parser.add_option( '-t', '--data-table-xml', dest='data_table_xml', type='string', default=DEFAULT_TOOL_DATA_TABLE_CONF, help='The name of the data table configuration file to get format of loc file [./%s]' % DEFAULT_TOOL_DATA_TABLE_CONF)
    parser.add_option( '-u', '--ucsc-build-txt', dest='ucsc_build_txt', type='string', default=DEFAULT_UCSC_BUILDS_TXT, help='The name of the UCSC build txt from which the dbkey name will be gathered [./%s]' % DEFAULT_UCSC_BUILDS_TXT)
    parser.add_option( '-s', '--ucsc-build-txt-skip', dest='ucsc_build_txt_skip', type='string', default=DEFAULT_UCSC_BUILDS_TXT_SKIP, help='lines to skip from ucsc builds.txt [%d]' % DEFAULT_UCSC_BUILDS_TXT_SKIP)
    parser.add_option( '-e', '--exemptions', dest='exemptions', type='string', default=EXEMPTIONS, help='Comma-separated list of subdirectories in GENOMES_DIR to not look in' )
    parser.add_option( '-i', '--inspect-dir', dest='inspect_dir', type='string', default=INSPECT_DIR, help='Comma-separated list of subdirectories inside GENOMES_DIR to look in (default is all)' )


    (options, args) = parser.parse_args()

    ####################################################################################################
    ## configure required user arguments
    (genomes_dir, out_dir) = args
    if args:
        if os.path.exists(genomes_dir):
            env.genomes_dir = genomes_dir
        else:
            print "genomes_dir: couldn't find path %s" % genomes_dir
            exit(1)
        if os.path.exists(out_dir):
            env.out_dir = out_dir
        else:
            print "out_dir: couldn't find path %s" %out_dir 
            exit(1)
    else:
        parser.print_help()
        exit(1)

    ####################################################################################################
    ####################################################################################################
    ### env settings

    ####################################################################################################
    ## tool data table xml file
    env.tool_data_table_conf_file=options.data_table_xml
    if not os.path.exists(env.tool_data_table_conf_file):
        print "tool_datable_xml: couldn't find path %s" % env.tool_data_table_conf_file
        exit(1)

    if options.inspect_dir:
        env.inspect = [ id for id in options.inspect_dir.split( ',' ) ]
    else:
        env.inspect = None


    ####################################################################################################
    ## exemptions
    exemptions = [ e.strip() for e in options.exemptions.split( ',' ) ]
    env.exemptions = exemptions

    ####################################################################################################
    ## Get uscs builds from tool-data/shared/ucsc/builds.txt
    ## This gives dbkey : Description 
    env.ucsc_build_txt_skip = options.ucsc_build_txt_skip
    env.ucsc_build_txt = options.ucsc_build_txt
    if not os.path.exists(env.ucsc_build_txt):
            print "ucsc_build_txt: couldn't find path %s" % env.ucsc_build_txt
            exit(1)

    ####################################################################################################

    ####################################################################################################
    ## genome dirs
    # all paths to look in
    genome_dirs = [ genomes_dir ]

    genomes_ucsc = _get_ucsc_genomes(env.ucsc_build_txt)
    env.dbkey_name_dict = genomes_ucsc

    ####################################################################################################
    ## tool managers
    env.tool_mngrs = {}
    for (tool, (mngr, indexdir)) in TOOLS.items():
        env.tool_mngrs[tool] = mngr(env)
        
    ####################################################################################################
    ## End of env variables
    ####################################################################################################

    ####################################################################################################
    ## Parse genome folder structure 
    print '\nSkipping the following:\n\t%s' % '\n\t'.join( exemptions )
    genomes = _get_genomes(genome_dirs)

    ####################################################################################################
    ## Parse potential tool folders in each genome folder 
    ## record attributes for filling out loc file
    #  @TODO For now we only want those that are in the ucsc build.txt
    filtered = []
    for (dbkey, dbkey_dir) in genomes:
        if dbkey in genomes_ucsc:
            filtered.append((dbkey,dbkey_dir))
        else:
            print "WARNING: Not found in ucsc build.txt. Skipping the dbkey",dbkey
    #print [(dbkey,dbkey_dir) for (dbkey,dbkey_dir) in genomes if (dbkey in genomes_ucsc)] 
    indexdirs = _get_indexdirs([(dbkey,dbkey_dir) for (dbkey,dbkey_dir) in genomes if (dbkey in genomes_ucsc)])
    #print indexdirs

    #for (tool, mngr) in env.tool_mngrs.items():
        #for (dbkey,dr) in indexdirs[mngr.indexdir]:
    for (tool,(mngr,indexdir)) in TOOLS.items():
        if indexdir in indexdirs:
            for (dbkey,dr) in indexdirs[indexdir]:
                #print "For %s Parsing %s" % (tool, dr)
                lines = env.tool_mngrs[tool].parse_indexdir(dbkey, dr)

                
####################################################################################################
## Helper functions

def _get_ucsc_genomes(build_txt):
    genomes_ucsc = {}
    with open(build_txt, 'rb') as buildfile:
        reader = csv.reader(buildfile, delimiter='\t')
        for i in range(0,env.ucsc_build_txt_skip):
            reader.next()

        for row in reader:
            genomes_ucsc[row[0]] = row[1]
    return genomes_ucsc

def _get_genomes(genome_dirs):
    genomes = []
    for genome_dir in genome_dirs:
        #print "Checking for genomes in %s" % genome_dir
        dbkeys = [ dr for dr in os.listdir( genome_dir ) if dr not in env.exemptions ]
        if env.inspect:
            dbkeys = [ key for key in dbkeys if key in env.inspect ]
        dbkeys_dir = map((lambda x: os.path.join(genome_dir , x)), dbkeys)
        genomes.extend(zip(dbkeys, dbkeys_dir))
    return genomes
        
def _get_indexdirs (genomes):
    valid_indexdirs = [ dr for (mngr, dr) in TOOLS.values() ]
    indexdirs = {}
    for (dbkey, dbkey_dir) in genomes:
        # find relevant tools
        for dr in os.listdir (dbkey_dir):
            # if the directory corresponds to a index directory we know
            # remember this directory
            if dr in valid_indexdirs:
                path = os.path.join(dbkey_dir,dr)
                if dr in indexdirs:
                    indexdirs[dr].append((dbkey,path))
                else:
                    indexdirs[dr] = [(dbkey,path)]
    return indexdirs 

class AttributeDict(dict):
    """
    Dictionary that allows attribute access to values.

    http://stackoverflow.com/questions/4984647/accessing-dict-keys-like-an-attribute-in-python
    """
    __getattr__ = dict.__getitem__
    __setattr__ = dict.__setitem__



if __name__=='__main__': __main__()


import optparse, os, sys
import subprocess
from multiprocessing import Process

EXEMPTIONS = 'bin,tmp,lengths,equCab2_chrM,microbes,download'
INSPECT_DIR = None
TWOBITTOFA = "twoBitToFa"
SEQ_SUB_DIR = "seq" 
env = {}
def __main__():
    global env 
    env = AttributeDict(env)

    # command line variables
    usage = "usage: %prog [options] GENOMES_DIR" 
    parser = optparse.OptionParser(usage=usage)
    parser.add_option( '-e', '--exemptions', dest='exemptions', type='string', default=EXEMPTIONS, help='Comma-separated list of subdirectories in GENOMES_DIR to not look in' )
    parser.add_option( '-i', '--inspect-dir', dest='inspect_dir', type='string', default=INSPECT_DIR, help='Comma-separated list of subdirectories inside GENOMES_DIR to look in (default is all)' )
    parser.add_option( '-t', '--threads', dest='threads', type='int', default=2, help='Number of simultaneous threads' )


    (options, args) = parser.parse_args()

    ####################################################################################################
    ## configure required user arguments
    (genomes_dir,) = args
    if args:
        if os.path.exists(genomes_dir):
            env.genomes_dir = genomes_dir
        else:
            print "genomes_dir: couldn't find path %s" % genomes_dir
            exit(1)
    else:
        parser.print_help()
        exit(1)

    env.threads = options.threads

    env.twoBitToFa = TWOBITTOFA
    ####################################################################################################
    ## exemptions
    exemptions = [ e.strip() for e in options.exemptions.split( ',' ) ]
    env.exemptions = exemptions

    ####################################################################################################
    ## genome dirs
    # all paths to look in
    if options.inspect_dir:
        genome_dirs = [ os.path.join( genomes_dir, id ) for id in options.inspect_dir.split( ',' ) ]
    else:
        genome_dirs = [ genomes_dir ]

    ####################################################################################################
    ## Parse genome folder structure 
    print '\nSkipping the following:\n\t%s' % '\n\t'.join( exemptions )
    genomes = _get_genomes(genome_dirs)

    ####################################################################################################
    ## Parse potential tool folders in each genome folder 
    ## record attributes for filling out loc file
    seqdirs = _get_seqdirs(genomes)

    groups = zip(*(iter(seqdirs),) * env.threads)

    for grp in groups:   
        ps = []
        for seqdr in grp:
            p = Process(target=_2bit_to_fa, args=(seqdr,))
            p.start()
            ps.append(p)

        for p in ps:
            p.join()
        print "Finished batch" 

    print "Finished all"
        #_2bit_to_fa(seqdr)

                
####################################################################################################
## Helper functions

def _2bit_to_fa(dr):
    # for each 2bit file
    for twobit in [ item for item in os.listdir(dr) if item.endswith(".2bit")]:

        fa = twobit.replace(".2bit",".fa")

        old = os.path.join(dr,twobit)
        new = os.path.join(dr,fa)
        print "Converting", old, "to", new
        #subprocess.call(["touch",new])
        #subprocess.call(["whoami"])
        #subprocess.call(["rm",new])
        #subprocess.call(["sleep","5"])
        subprocess.call([env.twoBitToFa, old, new])



def _get_genomes(genome_dirs):
    genomes = []
    for genome_dir in genome_dirs:
        #print "Checking for genomes in %s" % genome_dir
        dbkeys = [ dr for dr in os.listdir( genome_dir ) if dr not in env.exemptions ]
        dbkeys_dir = map((lambda x: os.path.join(genome_dir , x)), dbkeys)
        genomes.extend(zip(dbkeys, dbkeys_dir))
    return genomes
        
def _get_seqdirs (genomes):
    seqdirs = []
    for (dbkey, dbkey_dir) in genomes:
        # find relevant tools
        for dr in os.listdir (dbkey_dir):
            if dr == SEQ_SUB_DIR:
                path = os.path.join(dbkey_dir,dr)
                seqdirs.append(path)
    return seqdirs 

class AttributeDict(dict):
    """
    Dictionary that allows attribute access to values.

    http://stackoverflow.com/questions/4984647/accessing-dict-keys-like-an-attribute-in-python
    """
    __getattr__ = dict.__getitem__
    __setattr__ = dict.__setitem__



if __name__=='__main__': __main__()



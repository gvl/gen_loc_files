import os
import subprocess
import csv
from xml.etree import ElementTree


class _toolManager:
    ## Tool Manager
    #  Stores details specific for each tool
    #  Classes which inherit this should implement their own parse_indexdir
    def __init__(self, tool, tool_conf_name, env):
        self.tool      = tool
        self.name_dict = env.dbkey_name_dict
        self.out_dir   = env.out_dir
        self.tool_data_table_conf_file = env.tool_data_table_conf_file 
        self.tool_conf_name = tool_conf_name
        self.tool_conf = _get_tool_conf(self.tool_conf_name,self.tool_data_table_conf_file)

####################################################################################################
## Generic Managers

class _generic_mngr(_toolManager):
    def __init__(self, tool, subdir, tool_conf_name, base, env, unique_col="value", comment_char='#'):
        ## A generic manager eg for bwa, bowtie
        #  @param ext The extension to append in "path"
        _toolManager.__init__(self, tool, tool_conf_name, env)
        self.subdir = subdir
        self.unique_col = unique_col
        self.comment_char=comment_char
        (self.base_str, self.base_col) = base 

    def parse_indexdir(self, dbkey, dbkey_dir):
        ## Parse an index directory of indices
        #  Writes a relevant entry to the loc file
        config = {
                  "value"   : dbkey, 
                  "dbkey"   : dbkey,
                  "name"    : self.name_dict[dbkey],
                  "formats" : "index",
                  "line_type" : "index", 
                  "species" : ""
                 }
        
        try:
            tmp_base = self.base_str % config[self.base_col]
        except KeyError:
            tmp_base = self.base_str
        config["path"]  = os.path.abspath(os.path.join(dbkey_dir,self.subdir, tmp_base))
        line_parts = _build_galaxy_loc_line(config, True, self.tool_conf)
        out_f = self.tool_conf.get("file", "")
        unique_col_num = -1
        try:
            unique_col_num = self.tool_conf.get('columns', []).index(self.unique_col)
        except:
            print "Script couldn't figure out the unique column for this tool. Exiting"
            exit(2)

        _update_loc_file(out_f, line_parts, self.out_dir, unique_col_num, self.comment_char)

####################################################################################################
## Custom Tool Managers 
#  One is required for each tool
class all_fasta_mngr(_generic_mngr):
    ## all_fasta manager
    def __init__(self, env):
        _generic_mngr.__init__(self, "all_fasta", "", "all_fasta", ("%s.fa",("dbkey")), env)
        
class bowtie_mngr(_generic_mngr):
    ## bowtie manager
    def __init__(self, env):
        _generic_mngr.__init__(self, "bowtie", "", "bowtie_indexes", ("%s",("dbkey")), env)

class bowtie_cs_mngr(_generic_mngr):
    ## bowtie color space manager
    def __init__(self, env):
        _generic_mngr.__init__(self, "bowtie_cs", "cs", "bowtie_indexes_color", ("%s",("dbkey")), env)

class bwa_mngr(_generic_mngr):
    ## bwa manager
    def __init__(self, env):
        _generic_mngr.__init__(self, "bwa", "", "bwa_indexes", ("%s.fa",("dbkey")), env)

class bwa_cs_mngr(_generic_mngr):
    ## bwa manager
    def __init__(self, env):
        _generic_mngr.__init__(self, "bwa_cs", "../hg19full/bwa_index/cs", "bwa_indexes_color", ("%s.fa",("dbkey")), env)

class codingSnps_mngr(_generic_mngr):
    ## all_fasta manager
    def __init__(self, env):
        _generic_mngr.__init__(self, "codingSnps", "", "codingSnps", ("",()), env)

class gatk_mngr(_generic_mngr):
    ## gatk manager
    def __init__(self, env):
        _generic_mngr.__init__(self, "gatk", "", "gatk_picard_indexes", ("%s.fa",("dbkey")), env)

class lastz_mngr(_generic_mngr):
    ## lastz manager
    def __init__(self, env):
        _generic_mngr.__init__(self, "lastz", "", "lastz_seqs", ("%s.2bit",("dbkey")), env)

class mosaik_mngr(_generic_mngr):
    ## mosaik manager
    def __init__(self, env):
        _generic_mngr.__init__(self, "mosaik", "", "mosaik_indexes", ("%s.fa",("dbkey")), env)

class ngs_sim_mngr(_generic_mngr):
    ## ngs_sim manager
    def __init__(self, env):
        _generic_mngr.__init__(self, "ngs_sim", "", "ngs_sim_fasta", ("%s.fa",("dbkey")), env)

#class perm_mngr(_generic_mngr):
    ### perm manager
    #def __init__(self, env):
        #_generic_mngr.__init__(self, "perm", "", "perm_base_indexes", ".fa", env)

class picard_mngr(_generic_mngr):
    ## picard manager
    def __init__(self, env):
        _generic_mngr.__init__(self, "picard", "", "picard_indexes", ("%s.fa",("dbkey")), env)
        
class sam_mngr(_generic_mngr):
    ## sam manager
    def __init__(self, env):
        _generic_mngr.__init__(self, "sam", "", "sam_fa_indexes", ("%s.fa",("dbkey")), env)

class seq_index_base_mngr(_generic_mngr):
    ## sequence_index_base manager
    def __init__(self, env):
        _generic_mngr.__init__(self, "sequence_index_base", "", "sequence_index_base", ("%s.fa",("dbkey")), env)

class seq_index_color_mngr(_generic_mngr):
    ## sequence_index_color manager
    def __init__(self, env):
        _generic_mngr.__init__(self, "sequence_index_color", "../hg19full/bwa_index/cs", "sequence_index_color", ("%s.fa",("dbkey")), env)

class srma_mngr(_generic_mngr):
    ## srma manager
    def __init__(self, env):
        _generic_mngr.__init__(self, "srma", "", "srma_indexes", ("%s.fa",("dbkey")), env)

class twobit_mngr(_generic_mngr):
    ## mosaik manager
    def __init__(self, env):
        _generic_mngr.__init__(self, "twobit", "", "all_twobit", ("%s.2bit",("dbkey")), env)
####################################################################################################
## Helper functions
def _run(args):
    subprocess.call(args)

def _append(outf, add_str):
    f = open(outf, 'a')
    f.write(add_str)
    f.close()

def _contains(f, col_num, value, comment_char):
    #status = subprocess.call(["", string, f], stdout=open(os.devnull, 'w'))
    with open (f,'rb') as csvfile:
        reader = csv.reader(csvfile, delimiter='\t')
        for row in reader:
            if (not row[0].startswith(comment_char)) and (row[col_num] == value):
                return True
    return False


def _build_galaxy_loc_line(config, new_style, tool_conf):
    ## Prepare genome information to write to a Galaxy *.loc config file.
    if new_style:
        str_parts = []
        # Compose the .loc file line as str_parts list by looking for column values
        # from the retrieved tool_conf (as defined in tool_data_table_conf.xml). 
        for col in tool_conf.get('columns', []):
            str_parts.append(config.get(col, ""))
    else:
        str_parts = [config["dbkey"], config["path"]]
    return str_parts

def _update_loc_file(ref_file, line_parts, out_dir, unique_col_num, comment_char):
    ## Add a reference to the given genome to the base index file.
    # 
    if out_dir is not None:
        add_str = "\t".join(line_parts) + "\n"
        out_file = os.path.join(out_dir, ref_file)
        if not os.path.exists(out_file):
            _run(["touch", out_file])
        # Make sure there is no other line with same unique id
        if _contains(out_file, unique_col_num, line_parts[unique_col_num], comment_char):
            print "WARNING: The file:", out_file, "already contains the uniq id:", line_parts[unique_col_num], "(Skipped)"
        else:
            try:
                _append(out_file, add_str)
                #print "SUCCESS: Added to the file:", out_file, "and entry for the uniq id:", line_parts[unique_col_num]
            except:
                print "ERROR: couldn't add to the file :", out_file


def _get_tool_conf(table_name, conf_file):
    ## Parse the tool_data_table_conf.xml and extract
    #  values for the 'columns' tag and 'path' parameter for the 'file' tag, returning 
    #  those as a dict.
    tool_conf = {}
    tdtc = ElementTree.parse(conf_file)
    tables = tdtc.getiterator('table') 
    for t in tables:
        if table_name == t.attrib.get('name', ''):
            tool_conf['columns'] = t.find('columns').text.replace(' ', '').split(',')
            tool_conf['file'] = t.find('file').attrib.get('path', '')
    return tool_conf
